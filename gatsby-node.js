const path = require(`path`)
// Log out information after a build is done
exports.onPostBuild = ({ reporter }) => {
  reporter.info(`Your Gatsby site has been built!`)
}
// Create blog pages dynamically
exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions
  const pageTemplate = path.resolve(`src/templates/pageTemplate.js`)
  const result = await graphql(`
  query MyQuery {
    allFile(filter: {sourceInstanceName: {eq: "content-pages"}}) {
      edges {
        node {
          absolutePath
          id
          name
          publicURL
          relativeDirectory
          relativePath
          childMarkdownRemark {
            rawMarkdownBody
            frontmatter {
              date
              path
              title
            }
          }
        }
      }
    }
  }
  `)
  result.data.allFile.edges.forEach(edge => {
      let basePath = '/';
      if (edge.node.relativeDirectory.length > 0) {
          if (edge.node.name === 'index') {
            basePath = `${edge.node.relativeDirectory}/`;
          }else {
            basePath = `${edge.node.relativeDirectory}/${edge.node.name}`;
          }
      }else {
        if (edge.node.name === 'index') {
            basePath =`/`
        }else {
            basePath =`${edge.node.name}`
        }
      }
    createPage({
      path: basePath,
      component: pageTemplate,
      context: {
        filePath: edge.node.publicURL,
        id: `pages/${edge.node.relativePath}`,
        content: edge.node.childMarkdownRemark.rawMarkdownBody
      },
    })
  })
}