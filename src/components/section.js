import React from "react"

import "./layout.css"

const Section = ({ children }) => {

  return (
    <>
      <div 
        style={{
          padding: `32px`,
          backgroundColor: `rgba(0, 0, 0, .2)`,
        }}
      >
          I am a section
          {children}
      </div>
    </>
  )
}

export default Section