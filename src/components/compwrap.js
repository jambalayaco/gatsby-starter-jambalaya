import React, { Component } from "react";

class CompWrap extends Component {
  constructor() {
    super();
    this.onLocalStorageUpdate = this.onLocalStorageUpdate.bind(this);
    this.state = { jamdata: 'empty' };
  }
  componentDidMount() {
    if (typeof window !== 'undefined') {
      window.addEventListener('storage', this.onLocalStorageUpdate)
      }
  }
  componentWillUnmount(){
    if (typeof window !== 'undefined') {
      window.removeEventListener('storage', this.onLocalStorageUpdate)
    }
  }
  onLocalStorageUpdate() {
    const lsData = localStorage.getItem('jam');
    this.setState({jamdata: lsData});
  }

  render() {
    return (
        <div>
            I am a component
            <div>
              {this.state.jamdata}
            </div>
        </div>
    );
  }
}

export default CompWrap;