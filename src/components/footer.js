import React from "react"

const Footer = ({ siteTitle }) => (
  <footer>
      © {new Date().getFullYear()}
  </footer>
)

export default Footer
