import React, { Component } from "react";
import Section from '../components/section';

import unified from 'unified';
import remarkParse from 'remark-parse';
import remark2rehype from 'remark-rehype';
import rehype2react from 'rehype-react';
import sanitize from 'rehype-sanitize';

import rehypeRaw from 'rehype-raw';
import rehypeStringify from 'rehype-stringify';

import grayMatter from 'gray-matter';

import remark from 'remark';

class pageTemplate extends Component {
  constructor() {
    super();
    this.state = { 
      isEditMode: false,
      rawMd: '',
      frontMatter: '',
    };

    this.onLocalStorageUpdate = this.onLocalStorageUpdate.bind(this);
    this.toggleEditMode = this.toggleEditMode.bind(this);
  }

  componentDidMount() {
    if (typeof window !== 'undefined') {
      window.addEventListener('storage', this.onLocalStorageUpdate)
    }

    fetch(this.props.pageContext.filePath)
    .then(response => {
      return response.text()
    })
    .then(raw => {
      this.setTemplateContent(raw);
    })
  }

  componentWillUnmount(){
    if (typeof window !== 'undefined') {
      window.removeEventListener('storage', this.onLocalStorageUpdate)
    }
  }

  onLocalStorageUpdate() {
    const lsData = localStorage.getItem('jam');
    this.setState({jamdata: lsData});
  }

  toggleEditMode() {


    let check = remark().parse(this.state.rawMd)

    console.log(check);



    this.setState(state => ({
      isEditMode: !state.isEditMode
    }));
  }

  setTemplateContent(rawMd) {
    const markdown = grayMatter(rawMd);

    this.setState({
      frontMatter: markdown.data,
      rawMd:  markdown.content,
    })
  }

  render() {
    const processMarkdown = unified()
      .use(remarkParse, {commonmark: true}) // Parse
      .use(remark2rehype, {allowDangerousHTML: true}) // Convert to rehype
      .use(rehypeRaw) // Format
      .use(rehypeStringify) // Stringify
      .use(rehype2react, {
        createElement: React.createElement,
        components: {
          customc: Section,
        }
      })  // React
      //.use(sanitize) // Sanatize

    return (
        <div>
            <div>
                <div>
                  {this.props.pageContext.id}
                </div>
                <button onClick={this.toggleEditMode}>Turn edit {this.state.isEditMode ? 'On' : 'Off'}</button>
            </div>
            <div>
              {processMarkdown.processSync(this.props.pageContext.content).contents}
            </div>
        </div>
    );
  }
}

export default pageTemplate;
